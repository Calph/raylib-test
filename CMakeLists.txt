cmake_minimum_required(VERSION 3.29.0)
project(raylib_test CXX)
set(CMAKE_CXX_STANDARD 20)

#Adding Raylib
include(FetchContent)
set(FETCHCONTENT_QUIET FALSE)
set(BUILD_EXAMPLES OFF CACHE BOOL "" FORCE) # don't build the supplied examples
set(BUILD_GAMES    OFF CACHE BOOL "" FORCE) # don't build the supplied example games

FetchContent_Declare(
    raylib
    GIT_REPOSITORY "https://github.com/raysan5/raylib.git"
    GIT_TAG "master"
    GIT_PROGRESS TRUE
)

FetchContent_MakeAvailable(raylib)

# Add Executable
add_executable(${PROJECT_NAME} src/main.cpp)
#target_compile_options(${PROJECT_NAME})
target_include_directories(${PROJECT_NAME} PRIVATE "${CMAKE_SOURCE_DIR}/include")
target_link_directories(${PROJECT_NAME} PRIVATE "${CMAKE_SOURCE_DIR}/lib")
target_link_libraries(${PROJECT_NAME}
	PRIVATE raylib
	PRIVATE -lraylib
	PRIVATE -lopengl32
	PRIVATE -lgdi32
	PRIVATE -lwinmm
)

set(BuildName ${PROJECT_NAME})

set_target_properties( ${PROJECT_NAME}
	PROPERTIES
    ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/_build"
    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/_build"
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}/_build"
	OUTPUT_NAME ${BuildName}
)